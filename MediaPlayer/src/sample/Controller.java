package sample;

import javafx.animation.*;
import javafx.embed.swing.SwingFXUtils;
import javafx.geometry.Pos;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.WritableImage;
import javafx.scene.input.*;
import javafx.scene.media.*;
import javafx.scene.layout.*;
import javafx.stage.*;
import javafx.util.Duration;

import javax.imageio.ImageIO;
import java.awt.*;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalTime;

public class Controller {

    // Variables
    public Button playButton, forwardButton, rewindButton, closeButton, minimizeButton, fullscreenButton, defaultButton;
    public MediaPlayer player;
    public Label time;
    public Slider videoSlider, audioSlider, rateSlider;
    public BorderPane borderPane;
    public Stage stage;
    public Timeline timeline;
    public HBox centerHBox;
    public MediaView media;
    public Media mediaSource;

    boolean playing = false;
    boolean first = true;
    double minRes = 610;

    // Additional methods
    public String setLabelTime(double duration){
        int secs = (int)duration;
        int hours = (int)Math.floor(secs/3600);
        secs = (int)Math.floor(secs-(hours*3600));
        int mins = (int)Math.floor(secs/60);
        secs = (int)Math.floor(secs - (mins*60));
        LocalTime localTime = LocalTime.of(hours,mins,secs);
        return localTime.toString();
    }

    // Timeline update
    public void updateLabelTime(){ time.setText(setLabelTime(player.getCurrentTime().toSeconds())+" / "+setLabelTime(player.getCycleDuration().toSeconds())); }
    public void updateVideoSliderValue(){ videoSlider.setValue(player.getCurrentTime().toMillis()); }

    // Button action
    public void playButtonClick(){
        if (first) {
            videoSlider.setMin(0);
            videoSlider.setMax(player.getCycleDuration().toMillis());
            first = false;
        }
        playing = !playing;
        if (playing) {
            playButton.setText("❚❚");
            player.play();
            timeline = new Timeline();
            timeline.setCycleCount(Timeline.INDEFINITE);
            timeline.getKeyFrames().add(new KeyFrame(Duration.seconds(1), event -> updateLabelTime()));
            timeline.getKeyFrames().add(new KeyFrame(Duration.millis(500), event -> updateVideoSliderValue()));
            timeline.play();
        } else {
            playButton.setText("►");
            player.pause();
        }
    }
    public void forwardButtonClick(){
        player.seek(player.getCurrentTime().add(Duration.seconds(10)));
    }
    public void rewindButtonClick(){
        player.seek(player.getCurrentTime().add(Duration.seconds(-10)));
    }
    public void minimizeButtonClick(){
        stage = (Stage)(minimizeButton.getScene().getWindow());
        stage.setIconified(true);
    }
    public void closeButtonClick(){
        stage = (Stage) closeButton.getScene().getWindow();
        stage.close();
    }
    boolean fullscreen = false;
    public void fullscreenButtonClick(){
        fullscreen = !fullscreen;
        stage = (Stage)(fullscreenButton.getScene().getWindow());
        stage.setFullScreenExitKeyCombination(KeyCombination.NO_MATCH);
        stage.setFullScreen(fullscreen);
        if (fullscreen)
        media.setFitHeight(centerHBox.getHeight());
        else
        media.setFitHeight(minRes);
    }
    public void defaultButtonClick(){
        rateSlider.setValue(1);
        player.setRate(1);
    }

    // Slider action
    public void handleAudioSliderDrag(){ player.setVolume(audioSlider.getValue()/100); }
    public void handleRateSliderDrag(){ player.setRate(rateSlider.getValue()); }
    public void handleVideoSliderDrag(){
        if (first) {
            videoSlider.setMin(0);
            videoSlider.setMax(player.getCycleDuration().toMillis());
            first = false;
        }
        player.seek(Duration.millis(videoSlider.getValue()));
        videoSlider.setValue(player.getCurrentTime().toMillis());
    }

    // Menu action
    public void handleFileOpen(){
        player.dispose();
        first = true;
        FileChooser fileChooser = new FileChooser();
        File file = fileChooser.showOpenDialog(stage);
        if (file != null) {
            try {
                mediaSource = new Media(file.toURI().toURL().toExternalForm());
            }
            catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }
        player = new MediaPlayer(mediaSource);
        media = new MediaView(player);
        media.setFitHeight(centerHBox.getHeight());
        centerHBox = new HBox(media);
        centerHBox.setAlignment(Pos.CENTER);
        centerHBox.setStyle("-fx-background-color: black");
        borderPane.setCenter(centerHBox);
    }
    public void handleOpenAbout() {
        try {
            Desktop.getDesktop().browse(new URI("https://docs.oracle.com/javase/8/javafx/api/toc.htm"));
        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
        }
    }
    public void handleSnapshot() {
        WritableImage image = media.snapshot(new SnapshotParameters(), null);
        File file = new File(player.getCurrentTime()+".png");
        try {
            ImageIO.write(SwingFXUtils.fromFXImage(image, null), "png", file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // Key event
    public void handleKeyEvents(KeyEvent event){
        switch (event.getCode()){
            case W:
                playButtonClick();
                break;
            case A:
                rewindButtonClick();
                break;
            case D:
                forwardButtonClick();
                break;
            case F:
                fullscreenButtonClick();
                break;
            case PRINTSCREEN:
                handleSnapshot();
                break;
            case ESCAPE:
                closeButtonClick();
                break;
        }
    }
}
